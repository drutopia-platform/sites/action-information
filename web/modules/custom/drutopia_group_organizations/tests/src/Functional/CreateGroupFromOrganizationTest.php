<?php

namespace Drupal\Tests\drutopia_group_organizations\Functional;

use Drupal\node\Entity\Node;
use Drupal\group\Entity\GroupType;
use Drupal\user\Entity\User;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;
use Drupal\group\Entity\Storage\GroupRelationshipTypeStorageInterface;



/**
 * Tests the create_group_from_organization function.
 *
 * @group drutopia_group_organizations
 */
class CreateGroupFromOrganizationTest extends GroupBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'contextual',
    'shortcut',
    'tour',
    'toolbar',
    'contact',
    'gnode',
    'group',
    'block_content',
    'drutopia_event',
    'drutopia_organization',
    'drutopia_group_organizations',
  ];

    /**
   * The default theme used for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

    /**
   * The group type to run the tests with.
   *
   * @var \Drupal\group\Entity\GroupTypeInterface
   */
  protected $groupType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $group_type = GroupType::create([
      'id' => 'group',
      'label' => 'Group',
    ]);
    $group_type->save();
    $storage = $this->entityTypeManager->getStorage('group_content_type');
    assert($storage instanceof GroupRelationshipTypeStorageInterface);
    $storage->save($storage->createFromPlugin($group_type, 'group_node:organization'));
  }

  /**
   * Test the create_group_from_organization function.
   */
  public function testCreateGroupFromOrganization() {

    $user = User::create([
      'name' => 'testuser',
      'status' => 1,
    ]);
    $user->save();

    $node = Node::create([
      'type' => 'organization',
      'title' => 'Test Organization',
      'uid' => $user->id(),
    ]);
    $node->save();

    $groups = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['label' => 'Test Organization group']);
    $group = reset($groups);

    $this->assertNotNull($group, 'Group was created.');

    $this->assertEquals($user->id(), $group->getOwnerId(), 'Group has correct owner.');

    $relationships = $group->getRelationships();
    $this->assertCount(2, $relationships, 'Group has one relationship.');
    //$relationship = reset($relationships);
    $this->assertEquals($node->id(), $relationships[2]->getEntity()->id(), 'The relationship entity ID should match the node ID.');
    // 
  }

}
