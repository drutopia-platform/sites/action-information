<?php

/**
 * @file
 * Builds placeholder replacement tokens for saved searches.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\search_api\SearchApiException;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function drutopia_group_organizations_tokens_info(): array {
  $tokens['search-api-saved-search-results']['events'] = [
    'name' => t('Events'),
    'description' => t('List of event search results'),
  ];

  return [
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function drutopia_group_organizations_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()
      ->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }

  $replacements = [];

  if ($type === 'user' && !empty($data['user'])) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $data['user'];
  }

  if ($type === 'search-api-saved-search-results' && !empty($data['search_api_results'])) {
    /** @var \Drupal\search_api\Query\ResultSetInterface $results */
    $results = $data['search_api_results'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'events':
          $events = [];
          foreach ($results->getResultItems() as $item) {
            try {
              $datasource = $item->getDatasource();
              $object = $item->getOriginalObject();
              $title = $object->getEntity()->getTitle();
              $url = $datasource->getItemUrl($object);
              $url = $url->toString();
              $start_date = $object->getEntity()->field_date->getValue()[0]['value'];
              $end_date = $object->getEntity()->field_date->getValue()[0]['end_value'];
              $is_single_day_event =  date('Y-m-d', $start_date) == date('Y-m-d', $end_date);
              $start_date = date("D, M j, Y g:i A", $object->getEntity()->field_date->getValue()[0]['value']);
              if ($is_single_day_event) {
                $end_date = date('g:i A', $end_date);
              }
              else {
                $end_date = date("D, M j, Y g:i A", $end_date);
              }
              $date_string = $start_date . " - "  . $end_date;
              $events[] = "<li><a href=\"$url\">$title</a> ($date_string)</li>";
            }
            catch (SearchApiException $e) {
              Error::logException(\Drupal::logger('search_api_saved_searches'), $e, '%type while generating replacement tokens: @message in %function (line %line of %file).');
            }
          }
          $replacements[$original] = implode("\n", $events);
          break;
      }
    }
  }

  return $replacements;
}
