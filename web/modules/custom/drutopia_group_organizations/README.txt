Drutopia Group Organizations

- Automatically creates a group upon creation of a Drutopia organization. The autogenerated group will share the same name as the organization plus 'group' appended to the name (example: [Organization Name: 'Example Org', Group Name: 'Example Org group']).

- Provides event creation block from which the organization field in the event creation form will be preopulated with the organizations the user is associated wwith. The association of a user to an organization is determined by both the organization and user being members of the same group.
