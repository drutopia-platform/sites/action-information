(function (Drupal, once) {
  "use strict";

  Drupal.behaviors.toggleAddressFieldsRequired = {
    attach: function (context, settings) {

      const inperson_ids = [
        "edit-field-address-0-address-address-line1",
        "edit-field-address-0-address-locality",
        "edit-field-address-0-address-administrative-area",
        "edit-field-address-0-address-postal-code"
      ];

      const virtual_ids = [
        "edit-field-event-url-0-uri"
      ]

      function toggleRequiredAndClass(isRequired, inputIDs) {

        inputIDs.forEach(function (id) {
          const input = document.getElementById(id);
          if (input) {
            input.required = isRequired;
            const label = document.querySelector(`label[for="${id}"]`);
            if (label) {
              if (isRequired) {
                label.classList.add('form-required');
              } else {
                label.classList.remove('form-required');
              }
            }
          }
        });
      }


      // once('toggleAddressFieldsRequired', 'input[name="field_virtual_or_inperson[virtual]"]', context).forEach(function (checkbox) {
      //   toggleRequiredAndClass(checkbox.checked, virtual_ids);

      //   checkbox.addEventListener('change', function () {
      //     toggleRequiredAndClass(this.checked, virtual_ids);
      //   });
      // });
    }
  };

})(Drupal, once);