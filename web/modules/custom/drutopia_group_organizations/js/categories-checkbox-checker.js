(function (Drupal, drupalSettings) {
    "use strict";

    function termMatchesAcceptedPatterns(str) {
        const regex = /^c\[(c|sc|i)-\d+(-\d+)?(-\d+)?\]\[\d+\]$/;
        return regex.test(str);
    }
    
    let allCheckboxesContainer = document.getElementById('event-category-search-categories-with-children');
    let checkboxes = allCheckboxesContainer.querySelectorAll('input[type="checkbox"]');

    if (!termMatchesAcceptedPatterns(checkboxes[0].getAttribute('name'))) {
        throw new Error(
            "The javascript that prevents checking subcategories or issues if their parent is selected will not work.\n" +
            "The structure provided of the name attribute on the category/subcategory/issues is incompatible with this feature in" +
            "categories-checkbox-checker.js\n\n" +
            "The name attributes should fit the patterns c-71, sc-71-71, i-71-71-71 (the digits are only examples)\n" +
            `The structure of the name attribute of the category term provided is: ${checkboxes[0].getAttribute('name')}`
        )
    }

    function getCheckboxTermIds(checkbox) {
        let checkboxName = checkbox.getAttribute('name');
        let checkboxTermIds = checkboxName.slice(checkboxName.indexOf('[') + 1, checkboxName.indexOf(']')).split("-");
        return checkboxTermIds;
    }

    function defineSections(checkboxTermIds, checkBoxClasses) {
        let categoryID = checkboxTermIds[1];
        let subcategoryID = checkboxTermIds[2];

        let sectionNames = {'category':`category-${categoryID}`, 'subcategory':''};
        if (checkBoxClasses.includes('subcategory-checkbox') || checkBoxClasses.includes('issue-checkbox')) {
            sectionNames['subcategory'] = `subcategory-${categoryID}-${subcategoryID}`;
        }
        return {'sectionNames': sectionNames};
    }

    function preventClicking(event) {
        event.preventDefault();
    }

    for(let i=0; i<checkboxes.length; i++) {
        let checkBoxClasses = checkboxes[i].getAttribute('class').split(' ');
        let checkboxTermIds = getCheckboxTermIds(checkboxes[i]);
        let categories = defineSections(checkboxTermIds, checkBoxClasses);

        if (checkBoxClasses.includes('category-checkbox')) {
            let childBoxes = allCheckboxesContainer.getElementsByClassName(categories['sectionNames']['category']);
            for(let i=1; i<childBoxes.length; i++) {
                let categoryCheckBoxState = childBoxes[0].checked;
                if(categoryCheckBoxState == true) {
                    childBoxes[i].addEventListener('click', preventClicking, true);
                    document.querySelector(`label[for="${childBoxes[i].id}"]`).classList.toggle("parent-selected");
                }
            };
        };

        if (checkBoxClasses.includes('subcategory-checkbox')) {
            let childBoxes = allCheckboxesContainer.getElementsByClassName(categories['sectionNames']['subcategory']);

            for(let i=1; i<childBoxes.length; i++) {
                let subcategoryCheckBoxState = childBoxes[0].checked;
                if(subcategoryCheckBoxState == true) {
                    document.querySelector(`label[for="${childBoxes[i].id}"]`).classList.toggle("parent-selected");
                    childBoxes[i].addEventListener('click', preventClicking, true);
                }
            }
        }



        checkboxes[i].addEventListener("change", function (event) {
            let checkBoxClasses = this.getAttribute('class').split(' ');
            let checkboxTermIds = getCheckboxTermIds(this);
            let categories = defineSections(checkboxTermIds, checkBoxClasses);

            if (checkBoxClasses.includes('category-checkbox')) {
                let childBoxes = allCheckboxesContainer.getElementsByClassName(categories['sectionNames']['category']);
                let categoryCheckBoxState = childBoxes[0].checked;
                for(let i=1; i<childBoxes.length; i++) {
                    if(categoryCheckBoxState == true) {
                        document.querySelector(`label[for="${childBoxes[i].id}"]`).classList.add("parent-selected");
                        childBoxes[i].checked = false;
                        childBoxes[i].addEventListener('click', preventClicking, true);
                    }
                    else {
                        document.querySelector(`label[for="${childBoxes[i].id}"]`).classList.remove("parent-selected");
                        childBoxes[i].removeEventListener('click', preventClicking, true);
                    }
                };
            };
            if (checkBoxClasses.includes('subcategory-checkbox')) {
                let childBoxes = allCheckboxesContainer.getElementsByClassName(categories['sectionNames']['subcategory']);
                for(let i=1; i<childBoxes.length; i++) {
                    let subcategoryCheckBoxState = childBoxes[0].checked;
                    if(subcategoryCheckBoxState == true) {
                        document.querySelector(`label[for="${childBoxes[i].id}"]`).classList.add("parent-selected");
                        childBoxes[i].checked = false;
                        childBoxes[i].addEventListener('click', preventClicking, true);
                    }
                    else {
                        document.querySelector(`label[for="${childBoxes[i].id}"]`).classList.remove("parent-selected");
                        childBoxes[i].removeEventListener('click', preventClicking, true);
                    }
                }
            }
        })
    }
})(Drupal, drupalSettings);