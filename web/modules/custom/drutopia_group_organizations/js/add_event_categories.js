function openSummaryCallback(event) {
    event.preventDefault();
    var rect = this.getBoundingClientRect();
    var isInBefore = (event.clientX >= rect.left - 20 && event.clientX < rect.left);

    if (isInBefore) {
        var isExpanded = this.parentElement.getAttribute('open') === '';
        if (isExpanded) {
            this.parentElement.removeAttribute("open");
        }
        else {
            this.parentElement.setAttribute("open", "");
        }
    }
};

function limitSubcategoryCollapsibilityToPlusSign() {
    document.querySelectorAll('.subcategory-summary').forEach(function(element) {
        element.addEventListener('click', openSummaryCallback);
    });
}

document.addEventListener('DOMContentLoaded', () => {
    limitSubcategoryCollapsibilityToPlusSign();
    removeCategoriesLinks();
});

function issuesSelectorCallback() {
    selectIssues();
    selectSubcategories();
}

(document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);
    $trigger.addEventListener('click', issuesSelectorCallback);
});

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function removeCategoriesLinks()  {
    let categoriesAnchors = document.querySelectorAll(".category-list h2");
    for(let i=0; i<categoriesAnchors.length; i++) {
        categoriesAnchors[i].outerHTML = categoriesAnchors[i].outerHTML.replace(/<\/?a[^>]*>/g, "");
    };
}

function syncSameIssues(event) {
    let target = event.target;
    let splitRegex = new RegExp(`(?<=[\)\"]), `);
    let input = document.getElementById("edit-field-topics-target-id");
    let value = target.innerText;
    let regexValue = escapeRegExp(value);
    let issueID = target.parentElement.getAttribute('issue-id');
    let taxonomyRegex = new RegExp(`(")?${regexValue} \\(${issueID}\\)(")?`);
    let selectedIssuesList = document.querySelector("#selected-issues-list");
    let sameIssues = document.querySelectorAll(`[issue-id="${issueID}"]`);

    if (selectedIssuesList.contains(target)) {
        target.closest("li").remove();
        input.value = input.value.split(splitRegex).filter(substring => !taxonomyRegex.test(substring)).join(', ');
    }

    else if (target.closest(".issue-name").classList.contains("selected")) {
        input.value = input.value.split(splitRegex).filter(substring => !taxonomyRegex.test(substring)).join(', ');
    }

    else {
        if (input.value == "" && value.includes(",")) {
            input.value += `"${value} (${issueID})"`;
        }
        else if (input.value == "") {
            input.value += `${value} (${issueID})`;
        }
        else {
            if (value.includes(",")) {
                input.value += `, "${value} (${issueID})"`;
            }
            else {
                input.value += `, ${value} (${issueID})`;
            };
        };
        let newLi = document.createElement("li")
        newLi.classList.add("selected");
        newLi.appendChild(document.querySelector(`[issue-id="${issueID}"]`).cloneNode(true));
        selectedIssuesList.appendChild(newLi);
        newLi.getElementsByTagName("span")[0].addEventListener('click', syncSameIssues);
    }
    sameIssues = document.querySelectorAll(`[issue-id="${issueID}"]`);
    for (let j=0; j<sameIssues.length; j++) {
        sameIssues[j].classList.toggle("selected");
        if (!selectedIssuesList.contains(sameIssues[j]) && !sameIssues[j].closest('li').classList.contains("selected")) {
            sameIssues[j].closest('li').classList.toggle("selected");
        }
        else if (!selectedIssuesList.contains(sameIssues[j])){
            sameIssues[j].closest('li').classList.toggle("selected");
        }
        if (selectedIssuesList.contains(sameIssues[j]) && !sameIssues[j].classList.contains("selected")) {
            sameIssues[j].closest("li").remove();
        }
    }
};

function syncSameSubcategories(event) {
    let target = event.target;
    let splitRegex = new RegExp(`(?<=[\)\"]), `);
    let input = document.getElementById("edit-field-subcategories-target-id");
    let value = target.innerText;
    let regexValue = escapeRegExp(value);
    let subcategoryID = target.parentElement.getAttribute('subcategory-id');
    let taxonomyRegex = new RegExp(`(")?${regexValue} \\(${subcategoryID}\\)(")?`);
    let selectedSubcategoriesList = document.querySelector("#selected-subcategories-list");
    let sameSubcategories = document.querySelectorAll(`[subcategory-id="${subcategoryID}"]`);

    if (selectedSubcategoriesList.contains(target)) {
        target.closest("li").remove();
        input.value = input.value.split(splitRegex).filter(substring => !taxonomyRegex.test(substring)).join(', ');
    }

    else if (target.closest(".subcategory-name").classList.contains("selected")) {
        input.value = input.value.split(splitRegex).filter(substring => !taxonomyRegex.test(substring)).join(', ');
    }

    else {
        if (input.value == "" && value.includes(",")) {
            input.value += `"${value} (${subcategoryID})"`;
        }
        else if (input.value == "") {
            input.value += `${value} (${subcategoryID})`;
        }
        else {
            if (value.includes(",")) {
                input.value += `, "${value} (${subcategoryID})"`;
            }
            else {
                input.value += `, ${value} (${subcategoryID})`;
            };
        };
        let newLi = document.createElement("li")
        newLi.classList.add("selected");
        newLi.appendChild(document.querySelector(`[subcategory-id="${subcategoryID}"]`).cloneNode(true));
        selectedSubcategoriesList.appendChild(newLi);
        newLi.getElementsByTagName("span")[0].addEventListener('click', syncSameSubcategories);
    }
    sameSubcategories = document.querySelectorAll(`[subcategory-id="${subcategoryID}"]`);
    for (let j=0; j<sameSubcategories.length; j++) {
        sameSubcategories[j].classList.toggle("selected");
        if (!selectedSubcategoriesList.contains(sameSubcategories[j]) && !sameSubcategories[j].closest('li').classList.contains("selected")) {
            sameSubcategories[j].closest('li').classList.toggle("selected");
        }
        else if (!selectedSubcategoriesList.contains(sameSubcategories[j])){
            sameSubcategories[j].closest('li').classList.toggle("selected");
        }
        if (selectedSubcategoriesList.contains(sameSubcategories[j]) && !sameSubcategories[j].classList.contains("selected")) {
            sameSubcategories[j].closest("li").remove();
        }
    }
};

function selectIssues() {
    let selectedIssues = document.querySelectorAll(".issue-name.selected");
    let selectedIssuesList = document.querySelector("#selected-issues-list");
    let splitRegex = new RegExp(`(?<=[\)\"]), `);
    let input = document.getElementById("edit-field-topics-target-id");
    let inputValueArray = input.value.split(splitRegex);
    let issues = document.querySelectorAll(".issue-name");

    // sync selected issues with values in input
    for(let i=0; i<selectedIssues.length; i++) {
        if (input.value.indexOf(selectedIssues[i].getElementsByTagName("span")[0].innerText) === -1) {
            selectedIssues[i].classList.toggle("selected");
            if ((selectedIssuesList.contains(selectedIssues[i]))) {
                selectedIssues[i].closest("li").remove();
            }
        };
    };

    for(let i=0; i<issues.length; i++) {
        // if issue is in input value array and not selected, select it
        for(let index=0; index<inputValueArray.length; index++) {
            if (inputValueArray[index].indexOf(issues[i].getElementsByTagName("span")[0].innerText) !== -1 ) {
                if (!issues[i].classList.contains("selected")) {
                    issues[i].classList.toggle("selected");

                    selectedIssuesList = document.querySelector("#selected-issues-list");
                    if (selectedIssuesList.innerText.indexOf(issues[i].outerText) === -1) {
                        let newLi = document.createElement("li");
                        newLi.classList.add("selected");
                        let issueToBeCloned = document.querySelector(`[issue-id="${issues[i].getAttribute('issue-id')}"]`);
                        newLi.appendChild(issueToBeCloned.cloneNode(true));
                        selectedIssuesList.appendChild(newLi);
                        newLi.getElementsByTagName("span")[0].removeEventListener('click',  syncSameIssues);
                        newLi.getElementsByTagName("span")[0].addEventListener('click',  syncSameIssues);
                        issues[i].closest("li").classList.toggle("selected");
                    }
                    else {
                        issues[i].closest("li").classList.toggle("selected");
                    };
                };
            };
        };
        if (!selectedIssuesList.contains(issues[i]) && !issues[i].classList.contains("selected")) {
            issues[i].getElementsByTagName("span")[0].removeEventListener('click', syncSameIssues);
            issues[i].getElementsByTagName("span")[0].addEventListener('click', syncSameIssues);
        }
    };
};

function selectSubcategories() {
    let selectedSubcategories = document.querySelectorAll(".subcategory-name.selected");
    let selectedSubcategoriesList = document.querySelector("#selected-subcategories-list");
    let splitRegex = new RegExp(`(?<=[\)\"]), `);
    let input = document.getElementById("edit-field-subcategories-target-id");
    let inputValueArray = input.value.split(splitRegex);
    let subcategories = document.querySelectorAll(".subcategory-name");

    // sync selected subcategories with values in input
    for(let i=0; i<selectedSubcategories.length; i++) {
        if (input.value.indexOf(selectedSubcategories[i].getElementsByTagName("span")[0].innerText) === -1) {
            selectedSubcategories[i].classList.toggle("selected");
            if ((selectedSubcategoriesList.contains(selectedSubcategories[i]))) {
                selectedSubcategories[i].closest("li").remove();
            }
        };
    };

    for(let i=0; i<subcategories.length; i++) {
        // if subcategory is in input value array and not selected, select it
        for(let index=0; index<inputValueArray.length; index++) {
            if (inputValueArray[index].indexOf(subcategories[i].getElementsByTagName("span")[0].innerText) !== -1 ) {
                if (!subcategories[i].classList.contains("selected")) {
                    subcategories[i].classList.toggle("selected");

                    selectedSubcategoriesList = document.querySelector("#selected-subcategories-list");
                    if (selectedSubcategoriesList.innerText.indexOf(subcategories[i].outerHTML) === -1) {
                        let newLi = document.createElement("li");
                        newLi.classList.add("selected");
                        let subcategoryToBeCloned = document.querySelector(`[subcategory-id="${subcategories[i].getAttribute('subcategory-id')}"]`);
                        newLi.appendChild(subcategoryToBeCloned.cloneNode(true));
                        selectedSubcategoriesList.appendChild(newLi);
                        newLi.getElementsByTagName("span")[0].addEventListener('click', syncSameSubcategories);
                        subcategories[i].closest("li").classList.toggle("selected");
                    }
                    else {
                        subcategories[i].closest("li").classList.toggle("selected");
                    };
                };
            };
        };
        if (!selectedSubcategoriesList.contains(subcategories[i]) && !subcategories[i].classList.contains("selected")) {
            subcategories[i].getElementsByTagName("span")[0].removeEventListener('click', syncSameSubcategories);
            subcategories[i].getElementsByTagName("span")[0].addEventListener('click', syncSameSubcategories);
        }
    };
};