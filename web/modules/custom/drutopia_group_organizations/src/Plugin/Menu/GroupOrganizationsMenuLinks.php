<?php

namespace Drupal\drutopia_group_organizations\Plugin\Menu;


use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\user\Entity\User;
use Drupal\drutopia_group_organizations\CreateContentWithOrganization;


/**
 * Menu Links for Group Organizations 
 */
class GroupOrganizationsMenuLinks extends MenuLinkDefault {

    public function isEnabled() {
        $organizations = new CreateContentWithOrganization('group__event');
        $organizations = $organizations->getOrganizationsForCurrentUser();
        $pluginID = $this->getPluginId();
        $label_and_blank_form = in_array($pluginID, ['drutopia_group_organizations.add_event_label', 'drutopia_group_organizations.add_event_child']);
        if($organizations && $label_and_blank_form) {
            return true;
        }
        if (!isset($this->pluginDefinition['options']['query'])) {
            return false;
        }
        $org_id = $this->pluginDefinition['options']['query']['edit[field_organization][widget][form][entity_id]'];
        if (!$org_id) {
            return;
        }
        return in_array($org_id, array_keys($organizations));
    }
}