<?php

namespace Drupal\drutopia_group_organizations\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\drutopia_group_organizations\CreateContentWithOrganization;

/**
 * Provides a block.
 *
 * @Block(
 *   id = "createcontentwithorganization_block",
 *   admin_label = @Translation("Create Content with Group's Organizations links block"),
 *   deriver = "Drupal\drutopia_group_organizations\Plugin\Derivative\CreateContentWithOrganizationBlock"
 * )
 */

class CreateContentWithOrganizationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $CreateContentWithOrg = new CreateContentWithOrganization($this->getDerivativeId());
    return $CreateContentWithOrg->build();
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $ccwc = new CreateContentWithOrganization($this->getDerivativeId());
    return AccessResult::allowedIfHasPermission($account, $ccwc->permission());
  }

}
