<?php

namespace Drupal\drutopia_group_organizations\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drutopia_group_organizations\CreateContentWithOrganization;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for the Products.
 */
class GroupOrganizationsMenuLinks extends DeriverBase implements ContainerDeriverInterface {

   /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * Creates a GroupOrganizationsMenuLinks instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    // We are creating links that look like: node/add/event?edit[field_organization][widget][form][entity_id]=93
    $organizations = new CreateContentWithOrganization('group__event');
    $organizations = $organizations->getOrganizations();

    foreach ($organizations as $id => $organization) {
      $links[$id] = [
        'title' => $organization->label(),
        'options' => ['query' => ['edit[field_organization][widget][form][entity_id]' => $organization->id()]]
      ] + $base_plugin_definition;
    }

    return $links;
  }
}