<?php

namespace Drupal\drutopia_group_organizations\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\group\Entity\Controller\GroupTypeController;


class CreateContentWithOrganizationBlock extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $group_types = \Drupal::entityTypeManager()->getStorage('group_type')->loadMultiple();
    
    foreach ($group_types as $group_type_id => $group_type) {

      $installed_ids = \Drupal::service('group_relation_type.manager')->getInstalledIds($group_type);
      
      foreach ($installed_ids as $installed_id) {
        if (substr($installed_id, 0, 11) === 'group_node:') {
          $content_type_id = substr($installed_id, 11);
          $id = $group_type_id . '__' . $content_type_id;
          $this->derivatives[$id] = $base_plugin_definition;
          $this->derivatives[$id]['admin_label'] = $group_type->label() . ' ' . $content_type_id;
        }
      }
    }
  
    return $this->derivatives;
  }
}

