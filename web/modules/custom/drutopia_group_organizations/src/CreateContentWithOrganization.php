<?php

namespace Drupal\drutopia_group_organizations;

use Drupal\core\Url;
use Drupal\Core\Template\Attribute;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\user\Entity\User;

/**
 * Encapsulates useful methods for creating content with category.
 */
class CreateContentWithOrganization { // implements CcwcInterface {

  /**
   * The identification string for this create content with category object.
   * 
   * @var string
   */
  public $id;

  /**
   * The content (node) type machine name.
   * 
   * @var string
   */
  public $content_type;

  /**
   * The field machine name.
   * 
   * @var string
   */
  public $field_name;

  public function __construct($id) {
    $this->id = $id;
    $parts = explode('__', $id);
    if (count($parts) !== 2) {
      throw new \Exception("Invalid Create Content with Category ID: $id");
    }
    $this->content_type = $parts[1];
    $this->field_name = 'field_organization';
  }

  public function label() {
    return t("Create :content_type content with category :field", [':field' => $this->fieldLabel(), ':content_type' => $this->contentTypeLabel()]);
  }

  public function permission() {
    return 'create ' . $this->content_type;
  }

  public function fieldLabel() {
    return $this->getField()->label();
  }

  public function getField() {
    $entity_type = 'node';
    $bundle = $this->content_type;
    $field_name = $this->field_name;
    return \Drupal\field\Entity\FieldConfig::loadByName($entity_type, $bundle, $field_name);
  }

  public function contentTypeLabel() {
    return \Drupal::entityTypeManager()
      ->getStorage('entity_bundle:node')
      ->load($this->content_type)
      ->label();
  }

  public function build() {
    $build = [];

    $organizations = $this->buildOrganizations();
    if ($organizations) {
      // Make sure drupal_render() does not re-order the links.
      $build['#sorted'] = TRUE;
      // Make up a menu name.
      $menu_name = $this->id;
      // Add the theme wrapper for outer markup.
      // Allow menu-specific theme overrides.
      $build['#theme'] = 'menu__' . strtr($menu_name, '-', '_');
      $build['#menu_name'] = $menu_name;
      $build['#items'] = $organizations;
      // Set cache tag. - this should refresh when the vocabulary terms change, if we have a way to get that in Drupal.  Skipping for now.
      // $build['#cache']['tags'][] = 'config:system.menu.' . $menu_name;
    }

    return $build;
  }

  public function buildOrganizations() {
    $organizations = [];
    $organizations = $this->getOrganizationsForCurrentUser();
    foreach ($organizations as $key => $organization) {
      $element = [];
      $element['attributes'] = new Attribute();
      $element['title'] = $organization->getTitle();
      $element['url'] = $this->makeUrl($key);
      $organizations[$key] = $element;
    }
    return $organizations;
  }

  public function getOrganizationsForCurrentUser() {
    $uid = \Drupal::currentUser()->id();
    $user = User::load($uid);

    $group_organizations = [];
    $groups = [];
    $organizations = [];
    $group_memberships = \Drupal::service('group.membership_loader')->loadByUser($user);

    $role = 'group-org_rep';
    foreach ($group_memberships as $group) {
      if (isset($group->getRoles()[$role])) {
        $groups[] = $group->getGroup();
      }
    }

    // Get group content of type organization from user's groups.
    foreach (array_values($groups) as $group){
      if ($group->getRelatedEntities('group_node:organization')) {
        $group_organizations[] = $group->getRelatedEntities('group_node:organization')[0];
      };
    }

    // Get the nodes that the group content references.
    foreach (array_values($group_organizations) as $organization) {
      $organizations[$organization->id()] = $organization;
    }

    return $organizations;
  }

  public function getOrganizations() {

    $group_organizations = [];
    $groups = [];
    $organizations = [];
    $groups = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['type' => 'group']);

    // Get group content of type organization from user's groups.
    foreach (array_values($groups) as $group){
      if ($group->getRelatedEntities('group_node:organization')) {
        $group_organizations[] = $group->getRelatedEntities('group_node:organization')[0];
      };
    }

    // Get the nodes that the group content references.
    foreach (array_values($group_organizations) as $organization) {
      // $organization = $organization->getEntity();
      $organizations[$organization->id()] = $organization;
    }

    return $organizations;
  }

  public function makeUrl($term_id) {
    return Url::fromRoute('node.add', [
      'node_type' => $this->content_type,
    ], [
      'query' => [
        $this->prepopulateQueryKey() => $term_id,
      ],
    ]);
  }

  public function prepopulateQueryKey() {
    // The documented approach does not work for select lists
    // "edit[$this->field_name][widget][0][target_id]"
    // Below does work for select lists, though it's not documented.
    // @TODO look up what kind of field we have and produce a working
    // prepopulate link based on trial and error and the 'documentation'
    // https://git.drupalcode.org/project/prepopulate/blob/8.x-2.x/tests/src/Functional/PrepopulateFieldTest.php
    // Again note that what we do here is not in the tests nor documentation.
    return "edit[$this->field_name][widget][form][entity_id]";
  }
}
