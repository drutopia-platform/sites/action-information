<?php

namespace Drupal\drutopia_group_organizations\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface {

  private $account;

  public function __construct() {
    $this->account = \Drupal::currentUser();
  }

  public function checkAuthStatus(RequestEvent $event) {

    if ($this->account->isAnonymous() && \Drupal::routeMatch()->getRouteName() != 'user.login') {

      // add logic to check other routes you want available to anonymous users,
      // otherwise, redirect to login page.
      $route_name = \Drupal::routeMatch()->getRouteName();

      $restricted_calendar_routes = [
        'view.calendar.page_1',
        'view.calendar.page_2',
        'view.calendar.page_6',
        'view.calendar.page_7',
        'view.calendar.page_8',
        'view.calendar.page_9',
      ];
      if (in_array($route_name, $restricted_calendar_routes)) {
        $route_path =  Url::fromRoute($route_name)->toString();
        $response = new RedirectResponse("/user/login?destination=$route_path", 301);
        $event->setResponse($response);
      }
    }
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkAuthStatus');
    return $events;
  }
}
