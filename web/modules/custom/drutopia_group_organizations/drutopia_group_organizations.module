<?php

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRelationship;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Site\Settings;


function get_original_values(Node $node) {
  $event_preupdate = $node->original;
}

function create_group_from_organization($node) {
  $group = Group::create([
    'uid' => $node->getOwnerId(),
    'type' => 'group',
    'label' => $node->getTitle() . ' group',
  ]);
  $group->save();

  // Associate organization with group just created
  $group->addRelationship($node, 'group_node:' . $node->bundle());

  \Drupal::service('cache_tags.invalidator')->invalidateTags(['config:system.menu.main']);
  \Drupal::service('plugin.manager.menu.link')->rebuild();
}

function get_group_associated_with_node(Node $node, $update = false) {

  // Retrieve all groups associated with event
  $event_group_ids = [];

  $group_contents = GroupRelationship::loadByEntity($node);
  foreach ($group_contents as $group_content) {
    $event_group_ids[] = $group_content->getGroup()->id();
  }

  if ($event_group_ids) {
    \Drupal::logger('autocreate_group')->notice("Event :eid was already associated with a group.", [":eid" => $node->id()]);
    return;
  }

  // Retrieve first organization from event being updated
  $organization = $node->field_organization->entity;

  if (!$organization) {
    return;
  }

  // Retrieve group associated with organization
  $org_group_ids = [];

  $group_contents = GroupRelationship::loadByEntity($organization);
  foreach ($group_contents as $group_content) {
    $org_group_ids[] = $group_content->getGroup()->id();
  }
  if (!$org_group_ids && !$update) {
    \Drupal::logger('autocreate_group')->notice("No group associated with organization y:oid.", [":oid" => $organization->id()]);
    return;
  }
  elseif (!$org_group_ids && $update) {
    if (str_contains(\Drupal::request()->getRequestUri(), '/delete')) {
      return;
    }
    create_group_from_organization($organization);
    $group_contents = GroupRelationship::loadByEntity($organization);
    foreach ($group_contents as $group_content) {
      $org_group_ids[] = $group_content->getGroup()->id();
    }
    return ['event_group_id' => [], 'org_group_ids' => $org_group_ids, 'organization' => $organization];
  }


  // If organization and event are already in same group no need
  // to add organization to the event's group.
  if (array_intersect($event_group_ids, $org_group_ids)) {
    return;
  }

  $event_group_id = array_shift($event_group_ids);
  if ($event_group_ids) {
    \Drupal::logger('autocreate_group')
      ->notice(
        "Event :nid associated with more than one group discarded :group_ids.",
        [":group_ids" => implode(", ", $event_group_ids), ":id" => $node->id()]
      );
  }

  return ['event_group_id' => $event_group_id, 'org_group_ids' => $org_group_ids, 'organization' => $organization];
}

function add_organization_to_group(Node $node, $org_group_input) {
  $event_group_id = $org_group_input['event_group_id'];
  $organization = $org_group_input['organization'];

  // Add organization to event's group.
  $group = Group::load($event_group_id);
  /** @var \Drupal\group\Plugin\GroupContentEnablerInterface $plugin */
  $plugin = $group->getGroupType()->getContentPlugin('group_node:' . $node->bundle());
  $group_content = GroupContent::create([
    'type' => $plugin->getContentTypeConfigId(),
    'gid' => $group->id(),
    'entity_id' => $organization->id(),
  ]);
  $group_content->save();
}

function add_event_to_group(Node $node, $org_group_id) {

  $group = Group::load($org_group_id);
  $group->addRelationship($node, 'group_node:' . $node->bundle());
}

function get_group_associated_with_organization($node, $org_group_input) {
  $org_group_ids = $org_group_input['org_group_ids'];
  $organization = $org_group_input['organization'];

  if (!$org_group_ids) {
    \Drupal::logger('autocreate_group')
      ->notice(
        "Organization :oid associated with Event :eid was expected to be associated with a group but was not.",
        [":oid" => $organization->id(), ":eid" => $node->id()]
      );
    return;
  }
  $org_group_id = array_shift($org_group_ids);
  if ($org_group_ids) {
    \Drupal::logger('autocreate_group')
      ->notice(
        "Organization :oid associated with Event :eid is associated with more than group discarded :group_ids.",
        [":group_ids" => implode(", ", $org_group_ids), ":id" => $organization->id()]
      );
  }
  return $org_group_id;
}
/**
 * Implements drutopia_group_organizations_node_insert().
 * Automatically creates a group when an organization is created.
 * Associates the organization with the group after the group is created.
 */
function drutopia_group_organizations_node_insert(Node $node) {
  if ($node->bundle() == 'organization') {
    create_group_from_organization($node);
  }

  if ($node->bundle() == 'event') {

    if (!$node->field_image[0] && isset($node->get('field_organization')[0])) {
      if (isset($node->get('field_organization')[0]->get('entity')->getTarget()->getEntity()->field_image->target_id)) {
        $node->field_image = $node->get('field_organization')[0]->get('entity')->getTarget()->getEntity()->field_image;
        $node->save();
      }
    }

    if (str_contains(\Drupal::request()->getRequestUri(), 'group_node%3Aevent')) {
      return;
    }

    $associated_group = get_group_associated_with_node($node);

    if ($associated_group) {
      $org_group_id = get_group_associated_with_organization($node, $associated_group);
      add_event_to_group($node, $org_group_id);
    }
  }
}


/**
 * Implements hook_user_cancel_methods_alter().
 */
function drutopia_group_organizations_user_cancel_methods_alter(&$methods) {
    $methods['user_cancel_block']['description'] = "Your account will be deactivated and you will no longer be able to log in, however any content you created will remain active and attributed to your account.";
}

function drutopia_group_organizations_node_update(Node $node) {
  if ($node->bundle() == 'event') {

    if (!$node->field_image[0] && isset($node->get('field_organization')[0])) {

      if ($node->get('field_organization')[0]->get('entity')) {
        if (isset($node->get('field_organization')[0]->get('entity')->getTarget()->getEntity()->field_image->target_id)) {
          $node->field_image = $node->get('field_organization')[0]->get('entity')->getTarget()->getEntity()->field_image;
          $node->save();
        }
      }
    }
    $associated_group = get_group_associated_with_node($node, true);
    if ($associated_group) {
      if ($associated_group['event_group_id']) {
        add_organization_to_group($node, $associated_group);
      }
      else {
        $org_group_id = get_group_associated_with_organization($node, $associated_group);
        add_event_to_group($node, $org_group_id);
      }
    }
  }
}

function drutopia_group_organizations_menu_links_discovered_alter(&$links) {

  $links['drutopia_group_organizations.add_child_event'] = [
    'title' => 'Organization',
    'description' => 'Organization',
    'route_name' => 'node.add',
    'route_parameters' => ['node_type' => 'event'],
    'parent' => 'drutopia_group_organizations.add_event',
    'menu_name' => 'main',
    'class' => 'Drupal\drutopia_group_organizations\Plugin\Menu\GroupOrganizationsMenuLinks',
  ];
}

function drutopia_group_organizations_get_categories_with_children() {

  $c = [];

  $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  $categories = $termStorage->loadByProperties(['vid' => 'category']);
  foreach ($categories as $category) {
    $c[$category->tid->value] = [
      'term' => $category,
      'children' => [],
    ];
    $subcategories = $termStorage->loadByProperties(['vid' => 'subcategory', 'field_category' => $category->tid->value]);
    foreach ($subcategories as $subcategory) {
      $c[$category->tid->value]['children'][$subcategory->tid->value] = [
        'term' => $subcategory,
        'children' => [],
      ];
      $issues = $termStorage->loadByProperties(['vid' => 'topics', 'field_subcategory' => $subcategory->tid->value]);
      foreach ($issues as $issue) {
        $c[$category->tid->value]['children'][$subcategory->tid->value]['children'][$issue->tid->value] = [
          'term' => $issue,
        ];
      }
    }
  }
  return $c;
}

function make_saved_search_block_visible(&$form) {
  // Adds a hidden field to the form so that a query param is added which can be used with Drupal condition_query
  // to control the rendering of a block based on the presence of query params.

  $form['saved-search-visibility'] = array(
    '#type' => 'hidden',
    '#title' => 'title',
    '#default_value' => 'show',
    );
}

function build_category_subcategory_issues_nested_checkboxes(&$form) {
  $categories_with_children = drutopia_group_organizations_get_categories_with_children();
  $category_count = 0;
  //Hardcoded for now because only example, if generalized we can build the id from the view id and view display id
  $form['c']['#tree'] = TRUE;
  $form['c']['#prefix'] = '<section id="category-selector"><div class="category-toggle is-flex"></div><ul id="event-category-search-categories-with-children" class="category-with-children-list">';
  $form['c']['#suffix'] = '</ul></section>';

  foreach ($categories_with_children as $category_id => $category_holder) {
    $category = $category_holder['term'];
    $form['c']['c-' . $category_id]['#type'] = 'checkboxes';
    $form['c']['c-' . $category_id]['#options'] = [$category_id => $category->name->value];
    $form['c']['c-' . $category_id]['#attributes'] = ['class' => ['category-checkbox', 'category-' . $category_id]];
    $form['c']['c-' . $category_id]['#prefix'] = '<li><details><summary class="category-dropdown checkbox-dropdown">';

    $subcategory_count = 0;
    if (count($category_holder['children'])) {
      $form['c'][$category_id]['#suffix'] = '</summary><ul>';
      foreach ($category_holder['children'] as $subcategory_id => $subcategory_holder) {
        $subcategory = $subcategory_holder['term'];
        $subcategory_total = count($category_holder['children']);
        $form['c']['sc-' . $category_id . '-' . $subcategory_id]['#type'] = 'checkboxes';
        $form['c']['sc-' . $category_id . '-' . $subcategory_id]['#options'] = [$subcategory_id => $subcategory->name->value];
        $form['c']['sc-' . $category_id . '-' . $subcategory_id]['#attributes'] = ['class' => ['subcategory-checkbox', 'category-' . $category_id, 'subcategory-' . $category_id . '-' . $subcategory_id]];
        $form['c']['sc-' . $category_id . '-' . $subcategory_id]['#prefix'] = '<li><details><summary class="checkbox-dropdown">';

        $issue_count = 0;
        if (count($subcategory_holder['children'])) {
          $form['c'][$category_id . '-' . $subcategory_id]['#suffix'] = '</summary><ul>';
          foreach ($subcategory_holder['children'] as $issue_id => $issue_holder) {
            $issue = $issue_holder['term'];
            $issue_total = count($subcategory_holder['children']);
            $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#type'] = 'checkboxes';
            $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#options'] = [$issue_id => $issue->name->value];
            $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#attributes'] = ['class' => ['issue-checkbox', 'category-' . $category_id, 'subcategory-' . $category_id . '-' . $subcategory_id]];
            $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#prefix'] = '<li>';
            $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#suffix'] = '</li>';

            // At end of subcategory section but not end of category
            if (($issue_count == ($issue_total - 1)) && ($subcategory_count != ($subcategory_total - 1))) {
              $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#suffix'] .= '</ul></details></li>';
            }

            // At end of subcategory section and category section
            if (($issue_count == ($issue_total - 1)) && ($subcategory_count == ($subcategory_total - 1))) {
              $form['c']['i-' . $category_id . '-' . $subcategory_id . '-' . $issue_id]['#suffix'] .= '</ul></details></li></ul></details></li>';
            }
            $issue_count += 1;
          }
          $subcategory_count += 1;
        }
        else {
          $form['c']['sc-' . $category_id . '-' . $subcategory_id]['#suffix'] = '</summary></details></li>';
          if ($subcategory_count == ($subcategory_total - 1)) {
            $form['c']['sc-' . $category_id . '-' . $subcategory_id]['#suffix'] = '</summary></details></li></ul></details></li>';
          }
        }
      }
      $category_count += 1;
    }
    else {
      $form['c']['c-' . $category_id]['#suffix'] = '</summary></details></li>';
    }
  }
}

function drutopia_group_organizations_form_views_exposed_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $view = $form_state->getStorage('view');

  if (
    $form_id == 'views_exposed_form' &&
    $view['view']->id() == 'event' &&
    in_array($view['display']['id'], ['events_listing_list', 'events_listing_card'])
    ) {

    $form['#attached']['library'][] = 'drutopia_group_organizations/categories_checkbox_checker';
    $form['#attached']['library'][] = 'drutopia_group_organizations/autocomplete_places';
    $form['field_subcategory']['#access'] = $form['field_category']['#access'] = $form['field_topics']['#access'] = FALSE;
    $form['actions']['submit']['#attributes']['class'][] = $form['actions']['reset']['#attributes']['class'][] = 'mt-4';


    $form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['field_date_occurrence']['min']['#title'] = t('Event Start Date');
    $form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['field_date_occurrence']['max']['#title'] = t('Event End Date');

    $value = [
      'enabled' => [
        ':input[name="preselected_dates_occurrence"]' => ['value' => 'custom'],
      ],
    ];

    unset($form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['#title']);
    $form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['field_date_occurrence']['min']['#states'] = $value;
    $form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['field_date_occurrence']['max']['#states'] = $value;

    $form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['#attributes'][] = [
        'class' => 'flex-gap-2',
    ];

    $form['field_date_occurrence_wrapper']['field_date_occurrence_wrapper']['preselected_dates_occurrence'] = [
      '#type' => 'select',
      '#weight' => -1,
      '#title' => t('Event date'),
      '#options' => [
        'all' => t('All future events'),
        'today' => t('Events today'),
        'tomorrow' => t('Events tomorrow'),
        '7 days' => t('Next seven days'),
        '30 days' => t('Next thirty days'),
        'custom' => t('Custom'),
      ],
    ];

    make_saved_search_block_visible($form);
    build_category_subcategory_issues_nested_checkboxes($form);

    $form['field_geolocation_wrapper']['field_geolocation']['distance']['from']['#field_prefix'] = 'miles';
    $form['field_geolocation_wrapper']['field_geolocation']['value']['#field_prefix'] = 'location';
    $form['field_geolocation_wrapper']['field_geolocation']['value']['#size'] = 40;

    $form_validate = $form['#validate'];
    array_unshift($form_validate, 'drutopia_group_organizations_validate');
    $form['#validate'] = $form_validate;
  }

  if ($form['#id'] == 'views-exposed-form-org-reps-page-1') {
    $gids = \Drupal::entityQuery('group')->condition('type','Group')->accessCheck(TRUE)->execute();
    $groups = \Drupal\group\Entity\Group::loadMultiple($gids);
    $options = ['' => 'All'];

    foreach($groups as $gid => $group) {
      $value = $group->get('label')->getString();

      if (isset($value)) {
        $options[$gid] = $value; // Here the group ID is the value of the "option"
      }
      asort($options);
    }

    // find the "filtername" in the "filter identifier" field in the filter settings
    if (isset($form['gid'])) {
      $form['gid']['#attached']['library'][] = 'tomselect/tomselect';
      $form['gid']['#attributes']['class'][] = 'tomselect';
      $form['gid']['#type'] = 'select';
      $form['gid']['#options'] = $options;
      $form['gid']['#size'] = 1;
    }
  }
}

function drutopia_group_organizations_validate(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $categories = $subcategories = $issues = [];
  $complete_form = $form_state->getCompleteForm();
  $c = $complete_form['c'];
  $preselected_dates = $form_state->getValue('preselected_dates_occurrence');

  if ($preselected_dates != 'custom') {

    if ($preselected_dates == 'all') {
      $form_state->setValue(['field_date_occurrence', 'min'], date('Y-m-d'));
    }

    if ($preselected_dates == 'today') {
      $form_state->setValue(['field_date_occurrence', 'min'], date('Y-m-d'));
      $form_state->setValue(['field_date_occurrence', 'max'], date('Y-m-d'));
    }

    if ($preselected_dates == 'tomorrow') {
      $tomorrow =  new DateTime('tomorrow');
      $form_state->setValue(['field_date_occurrence', 'min'], $tomorrow->format('Y-m-d'));
      $form_state->setValue(['field_date_occurrence', 'max'], $tomorrow->format('Y-m-d'));
    }

    if ($preselected_dates == '7 days') {
      $plus_seven_days = new DateTime('+7 days');
      $form_state->setValue(['field_date_occurrence', 'max'], $plus_seven_days->format('Y-m-d'));
      $form_state->setValue(['field_date_occurrence', 'min'], date('Y-m-d'));
    }

    if ($preselected_dates == '30 days') {
      $plus_thirty_days= new DateTime('+30 days');
      $form_state->setValue(['field_date_occurrence', 'max'], $plus_thirty_days->format('Y-m-d'));
      $form_state->setValue(['field_date_occurrence', 'min'], date('Y-m-d'));
    }
  }

  else {
    $end_date = $form_state->getValue(['field_date_occurrence', 'max']);
    $date = $form_state->getValue(['field_date_occurrence', 'min']);
    if ($end_date && !$date) {
    }
    else {
      try {
        $date = (new DateTime($date))->format('Y-m-d');
      }
      catch (Exception $e) {
        $form_state->setErrorByName('field_date_occurrence', 'Invalid date format');
      }
    }
    try {
      $end_date = (new DateTime($end_date))->format('Y-m-d');
      $form_state->setValue(['field_date_occurrence', 'max'], $end_date);
      $form_state->setValue(['field_date_occurrence', 'min'], $date);

    }
    catch (Exception $e) {
      $form_state->setErrorByName('field_date_occurrence', 'Invalid date format');
    }
  }

  foreach ($c as $key => $value) {
    // if key is a render array property skip it
    if (gettype($key) == 'string' && $key[0] == '#') {
      continue;
    }

    if (isset($value['#value'])) {
      if (substr_count($key, '-') == 1 && substr_count($key, 'c-') == 1 ) {
        $categories += $value['#value'];
      }
      elseif (substr_count($key, '-') == 2 && substr_count($key, 'sc-') == 1 ) {
        $subcategories += $value['#value'];
      }
      elseif (substr_count($key, '-') == 3 && substr_count($key, 'i-') == 1 ) {
        $issues += $value['#value'];
      }
    }
  }

  $form_state->setValue('field_category', $categories);
  $form_state->setValue('field_subcategory', $subcategories);
  $form_state->setValue('field_topics', $issues);
}
function drutopia_group_organizations_event_form_validate(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $field_virtualorinperson = $form_state->getValue('field_virtual_or_inperson');
  $inperson_checked = (
    (isset($field_virtualorinperson[0]) && $field_virtualorinperson[0]['value']  == 'inperson') ||
    (isset($field_virtualorinperson[1]) && $field_virtualorinperson[1]['value']  == 'inperson')
  );
  $virtual_checked = (
    (isset($field_virtualorinperson[0]) && $field_virtualorinperson[0]['value']  == 'virtual')
  );
  $address_value = $form_state->getValue('field_address');
  $address_empty = empty($address_value[0]['address']['address_line1']);
  $uri_value = $form_state->getValue('field_event_url')[0]['uri'];
  $url_title = $form_state->getValue('field_event_url')[0]['title'];

  if ($virtual_checked && empty($uri_value)) {
    $form_state->setErrorByName('field_event_url', t('URL field is required when the event is virtual.'));
  }

  if (empty(!$uri_value) && empty($url_title)) {
    $form_state->setValue(['field_event_url', 0, 'title'], 'Event URL');
  }

  if (empty($uri_value) && !empty($url_title)) {
    $errors = $form_state->getErrors();
    unset($errors['field_event_url][0][uri']);
    $form_state->clearErrors();
    foreach ($errors as $key => $error) {
      $form_state->setErrorByName($key, $error);
    }
    $form_state->setValue(['field_event_url', 0, 'title'], '');
  }
}


function common_node_event_form_alter(&$form) {
  $form['#attached']['library'][] = 'drutopia_group_organizations/node_add_event';
  $form['#attached']['library'][] = 'drutopia_group_organizations/autocomplete_places';
  $form['#attached']['library'][] = 'drutopia_group_organizations/add_event_issues';
  $form['actions']['submit']['#submit'][] = 'submit_event';

  if (!isset($form['field_event_url']['widget'][0]['uri']['#states']['required'])) {
    $form['field_event_url']['widget'][0]['uri']['#states']['required'] = [];
  }
  else {
    $state_definition = $form['field_event_url']['widget'][0]['uri']['#states']['required'];
    $form['field_event_url']['widget'][0]['uri']['#states']['required'] = [];
    $form['field_event_url']['widget'][0]['uri']['#states']['required'][] = $state_definition;
    $form['field_event_url']['widget'][0]['uri']['#states']['required'][] = 'or';
  }

  $form['field_event_url']['widget'][0]['uri']['#states']['required'][] = [
    ':input[name="field_virtual_or_inperson[virtual]"]' => ['checked' => TRUE],
  ];

  $form['field_topics']['widget']['target_id']['#attributes']['placeholder'] =
    t("Start typing to see matching issues, you can select multiple issues using commas. Or use the Browse button above");
  $form['field_subcategories']['widget']['target_id']['#attributes']['placeholder'] =
    t("Start typing to see matching subcategories, you can select multiple subcategories using commas. Or use the Browse button above.");

  $form['field_description']['#disabled'] = TRUE;
  $form['#validate'][] = 'drutopia_group_organizations_event_form_validate';

}

function drutopia_group_organizations_form_node_event_edit_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  common_node_event_form_alter($form);

  if (isset($form['field_source_url']['widget'][0]['uri']['#default_value'])) {
    $url_text = Html::escape($form['field_source_url']['widget'][0]['uri']['#default_value']);
    $form['field_source_url']['widget'][0]['uri'] = [
      '#type' => 'item',
      '#markup' => '<a href="' . $url_text . '" target="_blank">' . $url_text . '</a>',
      '#default_value' => $url_text,
      '#prefix' => '<div class="form-item">',
      '#suffix' => '</div>',
      '#title' => t('Source URL'),
    ];
  }
}
function drutopia_group_organizations_form_node_event_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  common_node_event_form_alter($form);
  $form['#title'] = t('Add Event');
  $form['field_organization']['widget']['#field_title'] = 'Organized by (can be multiple organizations)';
  $form['field_organization']['widget']['form']['entity_id']['#attributes']['placeholder'] = 'Add organization now';
  $form['field_organization']['widget']['form']['entity_id']['#attributes']['class'][] = 'event-form-add-org-input';
  $form['field_organization']['widget']['form']['actions']['ief_reference_save']['#attributes']['class'][] = 'ief-event-entity-submit';
  if (!isset($form['#node']->nid)) {
    $form['revision_information']['#access'] = FALSE;
  }
  $form['field_source_url']['#disabled'] = TRUE;
}
function submit_event(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

  $user = \Drupal::currentUser();
  $roles = $user->getRoles();
  $orgs = $form['field_organization']['widget']['entities'];

  $moderation_state = $form_state->getFormObject()->getEntity()->get('moderation_state')->value;

  if ($moderation_state != 'published' || !in_array('editor', $roles)) {
    return;
  }

  foreach ($orgs as $org) {
    if (isset($org['#entity']) && $org['#entity']->isNew()) {
      $form_state->getFormObject()->getEntity()->set('moderation_state', 'ready');
      $form_state->getFormObject()->getEntity()->save();
      $messenger = \Drupal::messenger();
      $messenger->addMessage('The event is unpublished and staged for review because you have associated the event with a new organization.');
    }
  }
}
function drutopia_group_organizations_inline_entity_form_table_fields_alter(&$fields, $context)  {
  unset($fields['status']);
}

/**
 * Implements theme_menu_local_tasks().
 */
function drutopia_group_organizations_menu_local_tasks_alter(&$data, $route_name, \Drupal\Core\Cache\RefinableCacheableDependencyInterface &$cacheability) {
  $currentUser = \Drupal::currentUser();
  if ($route_name === 'entity.dashboard.canonical') {
    $data['tabs'][0]['entity.dashboard.member'] = [
      '#theme' => 'menu_local_task',
      '#link' => [
        'title' => 'Member Dashboard',
        'url' => Url::fromRoute('entity.dashboard.canonical', ['dashboard' => 'member']),
      ],
    ];
    if ($currentUser->hasPermission('can view groups_dashboard dashboard')) {
      $data['tabs'][0]['entity.dashboard.groups_dashboard'] = [
        '#theme' => 'menu_local_task',
        '#link' => [
          'title' => 'Groups Dashboard',
          'url' => Url::fromRoute('entity.dashboard.canonical', ['dashboard' => 'groups_dashboard']),
        ],
      ];
    }
    if ($currentUser->hasPermission('can view supervisor dashboard')) {
      $data['tabs'][0]['entity.dashboard.supervisor'] = [
        '#theme' => 'menu_local_task',
        '#link' => [
          'title' => 'Supervisor Dashboard',
          'url' => Url::fromRoute('entity.dashboard.canonical', ['dashboard' => 'supervisor']),
        ],
      ];
    }
    if ($currentUser->hasPermission('can view manager_dashboard dashboard')) {
      $data['tabs'][0]['entity.dashboard.manager_dashboard'] = [
        '#theme' => 'menu_local_task',
        '#link' => [
          'title' => 'Manager Dashboard',
          'url' => Url::fromRoute('entity.dashboard.canonical', ['dashboard' => 'manager_dashboard']),
        ],
      ];
    }
  }
  if (in_array($route_name, ['view.calendar.page_1','view.calendar.page_2', 'view.calendar.page_3', 'view.calendar.page_4', 'view.calendar.page_5','view.calendar.page_6', 'view.calendar.page_7', 'view.calendar.page_8', 'view.calendar.page_9'])) {
    $local_tasks_source_view = 'view.calendar.page_1';
    $source_local_tasks = \Drupal::service('plugin.manager.menu.local_task')->getLocalTasksForRoute($local_tasks_source_view);
    $data['tabs'][0] = [];
    foreach($source_local_tasks[0] as $key => $value) {
      $data['tabs'][0][] = [
        '#theme' => 'menu_local_task',
        '#link' => [
          'title' => $value->getTitle(),
          'url' => Url::fromRoute($value->getRouteName()),
        ],
        '#weight' => $value->getWeight(),
      ];
    }
  };
}

function check_if_logged_in() {
  $user = \Drupal::currentUser();
  if ($user->id() == 0) {
    return false;
  }
  return true;
}

function drutopia_group_organizations_link_alter(&$variables) {

  $logged_in = check_if_logged_in();
  $current_url = Url::fromRoute('<current>');
  $current_path = $current_url->toString();
  $list_paths = ['/calendar/all-events-list', '/calendar/bookmarked/list', '/calendar/organization-events/list'];
  $card_paths = ['/calendar/all-events-card', '/calendar/bookmarked/card', '/calendar/organization-events/card'];

  if ($variables['text'] == 'My Calendar') {
      if (!$logged_in) {
        $variables['options']['query']['destination'] = $variables['url']->toString();
        $variables['url'] = Url::fromRoute('user.login');
      }
      else if (in_array($current_path, $list_paths)) {
        $variables['url'] = Url::fromRoute('view.calendar.page_8');
      }
      else if (in_array($current_path, $card_paths)) {
        $variables['url'] = Url::fromRoute('view.calendar.page_9');
      }
  }

  if ($variables['text'] == "My Organizations' Events") {
    if (!$logged_in) {
      $variables['options']['query']['destination'] = $variables['url']->toString();
      $variables['url'] = Url::fromRoute('user.login');
    }

    else if (in_array($current_path, $list_paths)) {
      $variables['url'] = Url::fromRoute('view.calendar.page_6');
    }
    else if (in_array($current_path, $card_paths)) {
      $variables['url'] = Url::fromRoute('view.calendar.page_7');
    }
  }

  if ($variables['text'] == 'All Events') {
    if (in_array($current_path, $list_paths)) {
      $variables['url'] = Url::fromRoute('view.calendar.page_4');
    }
    if (in_array($current_path, $card_paths)) {
      $variables['url'] = Url::fromRoute('view.calendar.page_5');
    }
  }

  if (in_array($current_path, ['/calendar/bookmarked/list', '/calendar/bookmarked/card'])) {
    if ($variables['text'] == 'My Calendar') {
      $variables['options']['attributes']['class'][] = 'is-active';
    }
  }

  // if page is /events/bookmarked/organization-events-list /events/bookmarked/organization-events-card Organization Events is active
  if (in_array($current_path, ['/calendar/organization-events/list', '/calendar/organization-events/card'])) {
    if ($variables['text'] == "My Organizations' Events") {
      $variables['options']['attributes']['class'][] = 'is-active';
    }
  }

  // If page is /events/bookmarked/all-events-list /events/bookmarked/all-events-card All Events is active
  if ($variables['text'] == 'All Events') {
    if (in_array($current_path, ['/calendar/all-events-list', '/calendar/all-events-card'])) {
        $variables['options']['attributes']['class'][] = 'is-active';
    }
  }

  if ($variables['text'] != 'Calendar') {
    return;
  }

  $calendar_display_pages = ['/calendar', '/calendar/bookmarked', '/calendar/organization-events'];
  $calendar_display_classes = ['views-display-link-page_1', 'views-display-link-page_2', 'views-display-link-page_3'];
  $element_classes = [];
  if (isset($variables['options']['attributes']['class'])) {
    $element_classes = $variables['options']['attributes']['class'];
  }
  if (!empty(array_intersect($calendar_display_classes, $element_classes)) && in_array($current_path, $calendar_display_pages)) {
    $variables['options']['attributes']['class'][] = 'is-active';
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for user_pass().
 */
function drutopia_group_organizations_form_user_pass_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  // Change the label of the password field.
  if (isset($form['mail'])) {
    $form['mail']['#markup'] = t('Password reset instructions will be sent to your registered email address. (Check your junk folder if the email does not arrive in your inbox)');
  }
}
/**
 * Implements hook_form_FORM_ID_alter().
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 */
function drutopia_group_organizations_form_user_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $form['account']['name']['#access'] = FALSE;
  $form['account']['mail']['#required'] = TRUE;
}
function drutopia_group_organizations_search_api_saved_search_presave(EntityInterface $search): void {
  $originalPath = $search->getPath();
  $substringToRemove = "&saved-search-visibility=show";
  $cleanedPath = str_replace($substringToRemove, "", $originalPath);
  $search->set('path', $cleanedPath);
}

function drutopia_group_organizations_mail_alter(&$message) {
  if ($message['id'] == 'search_api_saved_searches_new_results') {
    $message['body'][0] = HTML::decodeEntities($message['body'][0]);
  }
}

/**
 * Implements hook_preprocess_page().
 */
function drutopia_group_organizations_page_attachments_alter(array &$attachments) {
  $attachments['#attached']['library'][] = 'drutopia_group_organizations/google_maps';
}

/**
 * Implements hook_library_info_alter().
 */
function drutopia_group_organizations_library_info_alter(array &$libraries, $extension) {
  if ($extension === 'drutopia_group_organizations' && isset($libraries['google_maps'])) {
    $api_key = Settings::get('google_maps_api_key');

    if ($api_key) {
      foreach ($libraries['google_maps']['js'] as $url => $info) {
        if (strpos($url, 'GOOGLE_MAPS_API_KEY') !== false) {
          $new_url = str_replace('GOOGLE_MAPS_API_KEY', $api_key, $url);
          unset($libraries['google_maps']['js'][$url]);
          $libraries['google_maps']['js'][$new_url] = $info;
        }
      }
    }
  }
}

function drutopia_group_organizations_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if (in_array($form_id, ["node_event_form", "node_event_edit_form"])) {
    $form['field_address']['widget']['#after_build'][] = 'drutopia_group_organizations_customize_address';
  }
}
function drutopia_group_organizations_customize_address($element, $form_state) {
  $element[0]['address']['organization']['#title'] = t("Building/Landmark/External Organization");
  $element[0]['address']['organization']['#description'] = t("Some addresses need additional details like East Capitol Lawn");
  return $element;
}