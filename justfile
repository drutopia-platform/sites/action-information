domain := ".actical.org"

#first command is treated as the default command
default:
  @echo 'Specify a command, or use -l to list options'

#edit the vault for the specified server
vault-edit server:
  ansible-vault edit host_vars/{{ server }}{{ domain }}/vault.yml

#update server software configuration and configured users (members).
update-server server:
  ansible-playbook -i inventory.yml hosting/provision.yml -l '{{ server }}{{ domain }}'

#update configured users (members).
update-users-only server:
  ansible-playbook -i inventory.yml hosting/provision.yml --skip-tags="setup,build,archive,push" -l '{{ server }}{{ domain }}'

update-build server build:
  ansible-playbook -i inventory.yml hosting/provision.yml -l '{{ server }}{{ domain }}' --tags=build,push,archive -e "target_build={{ build }}"

#update configured users (members).
update-individual-user server user:
  ansible-playbook -i inventory.yml hosting/provision.yml -l '{{ server }}{{ domain }}' --skip-tags="setup,build,archive,push" -e "target_member={{ user }}"

update-individual-user-forced server user:
  ansible-playbook -i inventory.yml hosting/provision.yml -l '{{ server }}{{ domain }}' --skip-tags="setup,build,archive,push" -e "target_member={{ user }} config_import_force_all=true"

#verify connectivity to all sites.
ping:
  ansible -i inventory.yml all -m ping

#ask each system to run a command, e.g. ahoy os-cmd 'lsb_release -a'. specify all for server to run everywhere
os-cmd server command:
  #!/usr/bin/env sh
  if [ "{{ server }}" = "all" ]; then
    ansible -i inventory.yml all -m shell -a "{{ command }}"
  else
    ansible -i inventory.yml {{ server }}{{ domain }} -m shell -a "{{ command }}"
  fi
