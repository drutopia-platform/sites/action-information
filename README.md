## Getting started

Prerequisites: Git and [DDEV](https://ddev.readthedocs.io/en/stable/#installation)

```bash
git clone git@gitlab.com:drutopia-platform/sites/action-information.git
cd action-information
ddev start
ddev auth ssh
```

Ensure your public SSH key is added added to [Gitlab.com](https://gitlab.com/-/profile/keys) and to [Drupal.org](https://git.drupalcode.org/-/profile/keys).

The domains for the test and live sites are:
https://actical-test.drutopia.org
https://actical.org

## Development

```bash
git pull
ddev composer update
ddev composer pull test
```

Get Live Database:

```bash
ddev drush sql-sync @live @self
```

Will give you the latest code from the repository and the current database from the test site.

## Deployment

The names in ansible vault for this site are:

  * `actical_test`
  * `actical_live`

First update the build:

```bash
just update-build main action_information
```

Then deploy to users (if any changes to config have occurred on the target user you will need to use update-individual-user-forced. Make sure to pull and commit any config you want to keep.):

```bash
just update-individual-user main actical_test
just update-individual-user main actical_live
```

To sync the database first (ssh aliases will only work if your local ssh config is configured to include Agaric's ssh config):

```bash
ssh actical-live
sync_to_test.sh
```

## Running tests
ddev exec ./vendor/bin/phpunit path/to/module
