Feature: Event Search Page
  Scenario: Events search page
    Given I am an anonymous user
    When I go to "/search"
    Then I should see the heading "Search Events"
  
  Scenario: Event search
    Given I am on "/search"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"

  Scenario: Event search with date
    Given I am on "/search"
    When I fill in "edit-field-date-occurrence-min" with "today"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"
    And I should see the text "Wed, Aug 14 2024, 9 - 10pm"

  Scenario: Event search with date
    Given I am on "/search"
    When I select "All future events" from "preselected_dates_occurrence"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"
    And I should see the text "Wed, Aug 14 2024, 9 - 10pm"

  Scenario: Event search with date
    Given I am on "/search"
    When I select "Events today" from "preselected_dates_occurrence"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"
    And I should see the text "Wed, Aug 14 2024, 9 - 10pm"

  Scenario: Event search with date
    Given I am on "/search"
    When I select "Events tomorrow" from "preselected_dates_occurrence"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"
    And I should see the text "Wed, Aug 14 2024, 9 - 10pm"

  Scenario: Event search with date
    Given I am on "/search"
    When I select "Next seven days" from "preselected_dates_occurrence"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"
    And I should see the text "Wed, Aug 14 2024, 9 - 10pm"

  Scenario: Event search with date
    Given I am on "/search"
    When I select "Next thirty days" from "preselected_dates_occurrence"
    When I press the "Search Events" button
    Then I should see the text "New Test Recurring Event"
    And I should see the text "Wed, Aug 14 2024, 9 - 10pm"