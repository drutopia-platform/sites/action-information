Feature: Home Page
  Scenario: Visit the home page
    Given I am an anonymous user
    When I go to "/"
    Then I should see the heading "Upcoming Events"
