

Feature: Calendar Page

# All Events Calendar Page

  Scenario: Calendar all events page
    Given I am an anonymous user
    When I go to "/calendar"
    Then I should see the heading "All Events"

  Scenario: Calendar all events page list view
    Given I am an anonymous user
    When I go to "/calendar/all-events-list"
    Then I should see the heading "All Events"

  Scenario: Calendar all events page card view
    Given I am an anonymous user
    When I go to "/calendar/all-events-card"
    Then I should see the heading "All Events"

# End all events calendar page

# Calendar bookmark page for anonymous users

  Scenario: Calendar bookmark page
    Given I am an anonymous user
    When I go to "/calendar/bookmarked"
    Then I should see the heading "Log in"

  Scenario: Calendar bookmark page list view
    Given I am an anonymous user
    When I go to "/calendar/bookmarked"
    Then I should see the heading "Log in"

  Scenario: Calendar bookmark page card view
    Given I am an anonymous user
    When I go to "/calendar/bookmarked"
    Then I should see the heading "Log in"

# End Calendar bookmark page for anonymous users

# Calendar bookmark page for users with the role volunteer

  @api
  Scenario: Calendar bookmark page
    Given I am logged in as a "volunteer"
    When I go to "/calendar/bookmarked"
    Then I should see the heading "My Calendar"

  Scenario: Calendar bookmark page list view
    Given I am an anonymous user
    When I go to "/calendar/bookmarked"
    Then I should see the heading "Log in"

  Scenario: Calendar bookmark page card view
    Given I am an anonymous user
    When I go to "/calendar/bookmarked"
    Then I should see the heading "Log in"

# End Calendar bookmark page for users with the role volunteer

# Calendar organization bookmark page for anonymous users

  Scenario: Calendar Organization bookmark page
    Given I am an anonymous user
    When I go to "/calendar/organization-events"
    Then I should see the heading "Log in"

  Scenario: Calendar Organization bookmark page
    Given I am an anonymous user
    When I go to "/calendar/organization-events/list"
    Then I should see the heading "Log in"

  Scenario: Calendar Organization bookmark page
    Given I am an anonymous user
    When I go to "/calendar/organization-events/card"
    Then I should see the heading "Log in"

# End calendar organization bookmark page for anonymous users