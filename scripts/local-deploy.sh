#!/bin/bash
# This script simulates what a deploy does AFTER it deployes the code.
# This will wipe out any local un-exported configuration.
set -e
# Confirmed with echo `pwd` that as long as we use via composer it's always in
# /var/www/html (aka the project root).
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.
[ "$proj_dir" != "/var/www/html" ] && { echo "Script running from unexpected path - are you running within ddev, as you should?"; exit 1; }
backup_file="$(date +%Y%m%dT%H%M%S)_pre_pull.sql"
[ ! -d /var/www/html/backups ] && mkdir /var/www/html/backups || true
echo "Backing up current db to backups/${backup_file}..."
drush sql-dump > /var/www/html/backups/${backup_file}
echo "Update database"
drush -y updb
echo "Import configuration"
drush -y cim
echo "Import custom string translations"
find ./translations/* -prune -type f -name "custom-translations.*.po" | while IFS= read -r d; do
    lang_code=${d: -5:-3}
    echo "drush locale-import ${lang_code} ./translations/${d} --type=customized --override=all"
done
echo "Cache reload"
drush cr
echo "Done"
