#!/bin/bash
set -euo pipefail

source_site_root="/home/action_information_live/site/web"
source_drush="/home/action_information_live/site/vendor/bin/drush -r ${source_site_root}"
source_files="${source_site_root}/sites/default/files/"

# Requires an .ssh/config entry for test-site. May be to localhost, and must not require a password for private key.
target_site=test-site
target_site_root="/home/action_information_test/site/web"
target_drush="/home/action_information_test/site/vendor/bin/drush -r ${target_site_root}"
target_files="${target_site_root}/sites/default/files/"
# Use ssh to target when target_site is set, otherwise use eval to just run in the local context
target_cmd="${target_site:+ssh ${target_site}}"
target_cmd="${target_cmd:-eval}"

sync_date=$(date +%Y%m%d_%H%M%S%Z)

# Test local drush/file paths/ssh connection/remote drush
[ -d ${source_files} ] || (echo "source_files folder does not exist"; exit 1)
${source_drush} status > /dev/null || (echo "Cannot execute local drush"; exit 1)
target_cmd "[ -d ${target_files} ]" || (echo "${target_cmd} failed, or ${target_files} folder does not exist at target. If using ssh, have you set up the key?"; exit 1)
target_cmd "${target_drush} status > /dev/null" || (echo "${target_cmd} ${target_drush} failed - does the site exist"; exit 1)

echo ">> All checks succeeded!"
echo ">> Syncing this site to ${target_site:-${target_site_root}} (using file date: ${sync_date})..."

# Dump destination DB (extra backup)
echo ">> Dumping destination database..."
target_cmd "${target_drush} sql-dump | gzip > ~/backups/${sync_date}-paranoia.sql.gz"
echo ">> Dumping source database..."
${source_drush} sql-dump | gzip  > ~/backups/${sync_date}-sync.sql.gz


# Send copy
echo ">> Copying source db to target..."
'scp' ~/backups/${sync_date}-sync.sql.gz ${target_site}:backups/


# Drop DB and reload from source
echo ">> Reloading target database..."
target_cmd "${target_drush} sql-drop -y"

target_cmd "gunzip ~/backups/${sync_date}-sync.sql.gz"
target_cmd "${target_drush} sqlc < ~/backups/${sync_date}-sync.sql"


# Copy files
echo ">> Sending new files..."
rsync -rlptvz --exclude=php --exclude=js --exclude=css --exclude=styles --exclude=twig ${source_files} ${target_site:+${target_site}:}${target_files}

echo ">> Sync to ${target_site:-${target_site_root}} complete - running cache rebuild..."

# CR forever!
target_cmd "${target_drush} cr"

echo ">> Target ${target_site:-${target_site_root}} is ready!"
