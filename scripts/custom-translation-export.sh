#!/bin/bash
# Add any languages your site needs to support.
# (Drutopia handles all languages automatically for import during deploy.)
set -e
# Confirmed with echo `pwd` that as long as we use via composer it's always in
# /var/www/html (aka the project root).
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.
[ "$proj_dir" != "/var/www/html" ] && { echo "Script running from unexpected path - are you running within ddev, as you should?"; exit 1; }
echo "Export English custom strings"
# This line is not needed once in configuration but it does not hurt.
drush cset locale.settings translation.path "../translations"
# Add extra languages manually, copy line and replace both 'en'.
drush -y locale:export en --types=customized > translations/custom-translations.en.po
echo "Done"
